// Question 2

function F = rhs(Y)
    x1 = Y(1, 1)
    x2 = Y(2, 1)
    val1 = - max(x1 - x2, 0)^(3/2)
    val2 =  max(x1 - x2, 0)^(3/2)
    F = [ Y(3, 1); Y(4,1); val1 ; val2]
endfunction


//Question 3
function [M] = eulerexp(Y, T, N)
    h = T/N
    Yk = Y
    for i = 1:4
        M(i,k) = Yk(i)
       end
    for k = 2:(N+1)
        Fk(1) = Yk(3)
        Fk(2) = Yk(4)
        temp = real((Yk(1)-Yk(2))^(5/2))
        Fk(3) = -max([temp,0])
        Fk(4) = max([temp,0])
        for i = 1:4
            Yk(i) = Yk(i) + h*Fk(i)
            M(i,k) = Yk(i)
       end
     end
endfunction

//Question 4
//Conditions initiales
Y = [0,0,1,0]
T = 4
N = [40,400,4000]

//Pour les 3 pas différents
for i = 1:3
    Ni = N(i)

    //Calcul des Yk => coordonnées
    M = eulerexp(Y, T, Ni)
    disp(M)
    y = M'
    disp(y)
    disp(y(1,:))
    X = linspace(0, T, Ni+1)
    
    //Graphique
    xtitle('Résolution avec le schéma Euler')
    for k = 1:4
        //Affichage de la courbe pour x1, x2, v1 et v2
        plot2d(X, M(k,:), k+1, axesflag=4)
    end
end
